
public class RecursiveUsingHelperMethods {
    /**
     * Given a string, compute recursively (no loops) the number of "11"
     * substring in the string. The "11" substrings should not overlap.
     * count11("11abc11") → 2 
     * count11("abc11x11x11") → 3 
     * count11("111") → 1
     */
    public static int count11(String str) {
        // Base case
        if (str.length() < 2) {
            return 0;
        }
        
        if (str.substring(0, 2).equals("11")) {
            // Removes first 2 characters
            return 1 + count11(str.substring(2));
        }
        // Removes just 1 character
        return count11(str.substring(1));
    }
    
    public static int count11_2(String str) {
        return count11Helper(str, 0);
    }
    
    public static int count11Helper(String str, int i) {
        // Base case
        if (i >= str.length() - 1) {
            return 0;
        } else if (str.charAt(i) == '1' && str.charAt(i + 1) == '1') {
            return 1 + count11Helper(str, i + 2);
        }
        return count11Helper(str, i + 1);
    }
    
    
    /**
     * Given a string, return true if it is a nesting of zero or more pairs of
     * parenthesis, like "(())" or "((()))". 
     * Suggestion: check the first and 
     * last chars, and then recur on what's inside them. 
     * nestParen("(())") → true 
     * nestParen("((()))") → true 
     * nestParen("(((x))") → false
     */
    public static boolean nestParen(String str) {
        // Base case
        if (str.isEmpty()) {
            return true;
        }
        
        if (str.charAt(0) == '(' && str.charAt(str.length() - 1) == ')') {
            return nestParen(str.substring(1, str.length() - 1));
        }
        return false;
    }
    
    public static boolean nestParen_2(String str) {
        return nestParenHelper(str, 0, str.length() - 1);
    }
    
    public static boolean nestParenHelper(String str, int lb, int rb) {
        // Base case
        if (lb > rb) {
            return true;
        }
        
        if (str.charAt(lb) == '(' && str.charAt(rb) == ')') {
            return nestParenHelper(str, lb + 1, rb - 1);
        }
        return false;
    }
    
    public static void main(String[] args) {
        System.out.println(count11("11abc11"));
        System.out.println(count11("abc11x11x11"));
        System.out.println(count11("111"));
        
        System.out.println("-".repeat(10));
        System.out.println(count11_2("11abc11"));
        System.out.println(count11_2("abc11x11x11"));
        System.out.println(count11_2("111"));
        
        System.out.println("-".repeat(10));
        System.out.println(nestParen("(())"));
        System.out.println(nestParen("((()))"));
        System.out.println(nestParen("(((x))"));
        
        System.out.println("-".repeat(10));
        System.out.println(nestParen_2("(())"));
        System.out.println(nestParen_2("((()))"));
        System.out.println(nestParen_2("(((x))"));
        
    }
}
